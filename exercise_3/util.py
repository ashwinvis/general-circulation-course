from datetime import timedelta
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def plot(
    df, V, suffix="", ax=None,
    ylabel="Spectral energy density ($m^3s^{{-2}}$)", 
    xlabel="Wavelength (km)",
    **kwargs
):
    if not ax:
        fig, ax = plt.subplots()
    df.plot(f"{V}{suffix}X", f"{V}{suffix}Y", ax=ax, **kwargs)
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    return ax

def wavenumber(wavelength_in_km):
    return 2*np.pi / (wavelength_in_km * 1e3)

def in_days(time_in_sec):
    # td = timedelta(seconds=time_in_sec)
    return time_in_sec / (24 * 60 * 60)

def printt(t):
    print(f"{t:.3g} seconds")
    d = in_days(t)
    print(f"{d:.3g} " + ("days" if d >= 2 else "day"))

def load():
    filename = "wpd_datasets.csv"
    df = pd.read_csv("wpd_datasets.csv", skiprows=(1,))

    # Shift meridional data one decade
    df.MeridionalX *= 10
    df.Meridional_fitX *= 10
    # Compute wavenumbers from wavelengths
    df["Zonal_k"] = wavenumber(df.ZonalX)
    df["Meridional_k"] = wavenumber(df.ZonalX)
    return df