# General circulation

All the assignments related to the doctral-level course General Circulation
offered in MISU.

## Installation

Using `pip-tools`

```sh
pip-sync
```

Using `conda` + `pip` / `pip-tools`

```sh
conda create --name <env> --file conda-requirements.txt
conda activate <env>
pip install -r requirements.txt
```

Using `pipenv`

```sh
pipenv install
```

## To sync

```sh
conda list --no-pip -e >! conda-requirements.txt # If you use conda
pip-sync # If you use pip-tools
pip freeze >! requirements.txt # If you use pip
```

## Exercises

1. Ocean circulation: Johan Nillson
2. Climate Sensitivity: Thorsten Mauritsen

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Creative Commons License" style="border-width:0"
  src="https://i.creativecommons.org/l/by/4.0/88x31.png" />
</a><br />
This work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
Creative Commons Attribution 4.0 International License
</a>.
