from dataclasses import dataclass
from uncertainties import ufloat
import numpy as np


def describe(y):
    """Describe a numpy array"""
    return f"array: {y.mean():.2f} +/- {y.std():.2f}"

def between(value, low, high):
    """Returns a boolean condition which marks it as True between two values."""
    return np.all([value>low, value<high], axis=0)
    

@dataclass()
class _ufloat:
    value: float = 0.
    uncertainty: float = 0.

    def __add__(self, other):
        result = self.__class__()
        result.value = self.value + other.value
        result.uncertainty = self.uncertainty + other.uncertainty
        return result

    def __truediv__(self, other):
        result = self.__class__()
        result.value = self.value / other.value
        result.uncertainty = self.value / other.value * (
            self.proportional_uncertainty + other.proportional_uncertainty)
        return result
    
    def __neg__(self):
        return self.__class__(-self.value, self.uncertainty)
        
    @property
    def proportional_uncertainty(self):
        return self.uncertainty / self.value

    

# From Table 1

# lambda_planck + lambda_water_lapse + lambda_albedo + lambda_cloud
lambdas = dict(
    planck      = ufloat(-3.2, 0.1),
    waterlapse = ufloat(1.15, 0.15),
    albedo      = ufloat(0.3, 0.15),
    cloud       = ufloat(0.38, 0.32)
)

forcing_co2 = ufloat(3.7, 0.3)

# From Table 2

delta_temp    = ufloat(0.77, 0.08)  # K
# The rest of the quantities are in Wm-2 units 
delta_forcing = ufloat(2.16, 0.59)

# Planetary imbalances
# 2005-2015
N2            = ufloat(0.71, 0.06)
# 1859-1882
N1            = ufloat(0.15, 0.075)

# forcing_change = dict(
#     ghg = ufloat(2.53, 0.18),
#     aerosol = ufloat(-0.69, 0.55),
#     black_carbon_on_snow = ufloat(0.02, 0.92),
#     strat_water_vapor = ufloat(0.06, 0.03),
#     land_use = ufloat(-0.10, 0.06),
#     ozone = ufloat(0.29, 0.12),
#     contrails = 0.05,
#     natural = -0.005
# )

delta_N = N2 - N1