    \hypertarget{monte-carlo-sampling-to-quantify-ecs}{%
\section{Monte-Carlo sampling to quantify
ECS}\label{monte-carlo-sampling-to-quantify-ecs}}

    To implement Monte-Carlo sampling,
\href{https://docs.pymc.io/}{PyMC3}(Salvatier, Wiecki, and Fonnesbeck
2016) was used which takes care of initialization of the random
distribution and random choice sampling in an elegant way. The
definitions of the probability distributions are done symbolically to
support lazy evaluation. It is also advantageous to use it, since it
supports parallel processing by out of the box.

To model the equations to solve for ECS, the input parameters were
initialized as normal distributions using the class
\texttt{pymc3.Normal}. Thereafter ECS was defined using the class
\texttt{pymc3.Deterministic}, since it has an algebraic relation with
the input parameters. The library was

PyMC3 defaults to a random sampler known as the \emph{No U-turn Sampler}
or \emph{NUTS} (Hoﬀman and Gelman, 2014) which was used to draw 100000
samples from each parameter iteratively and in parallel to compute ECS.

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}1}]:} \PY{k+kn}{import} \PY{n+nn}{pymc3} \PY{k}{as} \PY{n+nn}{pm}
        \PY{k+kn}{import} \PY{n+nn}{theano}
        \PY{k+kn}{import} \PY{n+nn}{theano}\PY{n+nn}{.}\PY{n+nn}{tensor} \PY{k}{as} \PY{n+nn}{tt}
        \PY{k+kn}{import} \PY{n+nn}{seaborn} \PY{k}{as} \PY{n+nn}{sns}
        \PY{k+kn}{from} \PY{n+nn}{uncertainties} \PY{k}{import} \PY{n}{ufloat}
        \PY{n}{sns}\PY{o}{.}\PY{n}{set}\PY{p}{(}\PY{n}{color\PYZus{}codes}\PY{o}{=}\PY{k+kc}{True}\PY{p}{)}
        
        
        \PY{k}{def} \PY{n+nf}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{quantity}\PY{p}{:} \PY{n}{ufloat}\PY{p}{,} \PY{n}{name}\PY{p}{:} \PY{n+nb}{str}\PY{p}{)}\PY{p}{:}
            \PY{l+s+sd}{\PYZdq{}\PYZdq{}\PYZdq{}Initializes and returns a normal distributiion.\PYZdq{}\PYZdq{}\PYZdq{}}
            \PY{k}{return} \PY{n}{pm}\PY{o}{.}\PY{n}{Normal}\PY{p}{(}
                \PY{n}{name}\PY{p}{,}
                \PY{n}{mu}\PY{o}{=}\PY{n}{quantity}\PY{o}{.}\PY{n}{nominal\PYZus{}value}\PY{p}{,}
                \PY{n}{sd}\PY{o}{=}\PY{n}{quantity}\PY{o}{.}\PY{n}{std\PYZus{}dev}\PY{p}{,}
            \PY{p}{)}
\end{Verbatim}


    \hypertarget{process-approach}{%
\subsection{Process-approach}\label{process-approach}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}2}]:} \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{forcing\PYZus{}co2}\PY{p}{,} \PY{n}{lambdas}
        
        \PY{k}{with} \PY{n}{pm}\PY{o}{.}\PY{n}{Model}\PY{p}{(}\PY{p}{)} \PY{k}{as} \PY{n}{model}\PY{p}{:}
            \PY{n}{df2x} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{forcing\PYZus{}co2}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}F\PYZus{}}\PY{l+s+si}{\PYZob{}2x\PYZcb{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dlambda} \PY{o}{=} \PY{p}{[}
                \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{l}\PY{p}{,} \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{lambda:}\PY{l+s+si}{\PYZob{}\PYZcb{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{o}{.}\PY{n}{format}\PY{p}{(}\PY{n}{key}\PY{p}{)}\PY{p}{)}
                \PY{k}{for} \PY{n}{key}\PY{p}{,} \PY{n}{l} \PY{o+ow}{in} \PY{n}{lambdas}\PY{o}{.}\PY{n}{items}\PY{p}{(}\PY{p}{)}
            \PY{p}{]}
            \PY{n}{ECS} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{Deterministic}\PY{p}{(}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{,} \PY{o}{\PYZhy{}}\PY{n}{df2x} \PY{o}{/} \PY{n}{tt}\PY{o}{.}\PY{n}{sum}\PY{p}{(}\PY{n}{dlambda}\PY{p}{)}\PY{p}{)}
        
        \PY{n}{model}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}2}]:}
    
    $$
            \begin{array}{rcl}
            \text{$F_{2x}$} &\sim & \text{Normal}(\mathit{mu}=3.7,~\mathit{sd}=0.3)\\\text{$\lambda:planck$} &\sim & \text{Normal}(\mathit{mu}=-3.2,~\mathit{sd}=0.1)\\\text{$\lambda:waterlapse$} &\sim & \text{Normal}(\mathit{mu}=1.15,~\mathit{sd}=0.15)\\\text{$\lambda:albedo$} &\sim & \text{Normal}(\mathit{mu}=0.3,~\mathit{sd}=0.15)\\\text{$\lambda:cloud$} &\sim & \text{Normal}(\mathit{mu}=0.38,~\mathit{sd}=0.32)\\\text{ECS} &\sim & \text{Deterministic}(\text{$F_{2x}$},~\text{$\lambda:planck$},~\text{$\lambda:waterlapse$},~\text{$\lambda:albedo$},~\text{$\lambda:cloud$})
            \end{array}
            $$

    

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}3}]:} \PY{k+kn}{import} \PY{n+nn}{os}
        \PY{k}{with} \PY{n}{model}\PY{p}{:}
            \PY{n}{trace} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{sample}\PY{p}{(}
                \PY{l+m+mi}{100}\PY{n}{\PYZus{}000}\PY{p}{,}
                \PY{n}{cores}\PY{o}{=}\PY{n}{os}\PY{o}{.}\PY{n}{cpu\PYZus{}count}\PY{p}{(}\PY{p}{)} \PY{o}{/}\PY{o}{/} \PY{l+m+mi}{2}
            \PY{p}{)}
\end{Verbatim}


    \begin{Verbatim}[commandchars=\\\{\}]
Auto-assigning NUTS sampler{\ldots}
Initializing NUTS using jitter+adapt\_diag{\ldots}
Multiprocess sampling (6 chains in 6 jobs)
NUTS: [\$\textbackslash{}lambda:cloud\$, \$\textbackslash{}lambda:albedo\$, \$\textbackslash{}lambda:waterlapse\$, \$\textbackslash{}lambda:planck\$, \$F\_\{2x\}\$]
Sampling 6 chains: 100\%|██████████| 603000/603000 [06:44<00:00, 440.84draws/s]

    \end{Verbatim}


    \hypertarget{sampled-values-and-their-distributions}{%
\subsubsection{Sampled values and their
distributions}\label{sampled-values-and-their-distributions}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}4}]:} \PY{o}{\PYZpc{}\PYZpc{}capture} \PYZhy{}\PYZhy{}no\PYZhy{}display
        \PY{o}{\PYZpc{}}\PY{n}{matplotlib} \PY{n}{inline}
        \PY{n}{axes} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{traceplot}\PY{p}{(}\PY{n}{trace}\PY{p}{)}
\end{Verbatim}


    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_11_0.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}5}]:} \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
        \PY{n}{pm}\PY{o}{.}\PY{n}{plots}\PY{o}{.}\PY{n}{forestplot}\PY{p}{(}\PY{n}{trace}\PY{p}{,} \PY{n}{rhat}\PY{o}{=}\PY{k+kc}{False}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}5}]:} GridSpec(1, 1)
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_12_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{input-parameters}{%
\subsubsection{Input parameters}\label{input-parameters}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}6}]:} \PY{o}{\PYZpc{}\PYZpc{}capture} \PYZhy{}\PYZhy{}no\PYZhy{}display
        \PY{o}{\PYZpc{}}\PY{n}{matplotlib} \PY{n}{inline}
        
        \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
        \PY{k}{for} \PY{n}{var} \PY{o+ow}{in} \PY{n}{trace}\PY{o}{.}\PY{n}{varnames}\PY{p}{:}
            \PY{n}{value} \PY{o}{=} \PY{n}{trace}\PY{o}{.}\PY{n}{get\PYZus{}values}\PY{p}{(}\PY{n}{var}\PY{p}{)}
            \PY{k}{if} \PY{n}{var} \PY{o}{!=} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:}
                \PY{n}{sns}\PY{o}{.}\PY{n}{distplot}\PY{p}{(}\PY{n}{value}\PY{p}{,} \PY{n}{label}\PY{o}{=}\PY{n}{var}\PY{p}{)}
        
        \PY{n}{plt}\PY{o}{.}\PY{n}{legend}\PY{p}{(}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}6}]:} <matplotlib.legend.Legend at 0x7f1a4a6a7da0>
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_14_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{ecs}{%
\subsubsection{ECS}\label{ecs}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}7}]:} \PY{k+kn}{import} \PY{n+nn}{numpy} \PY{k}{as} \PY{n+nn}{np}
        \PY{n}{ECS} \PY{o}{=} \PY{n}{trace}\PY{o}{.}\PY{n}{get\PYZus{}values}\PY{p}{(}\PY{l+s+s1}{\PYZsq{}}\PY{l+s+s1}{ECS}\PY{l+s+s1}{\PYZsq{}}\PY{p}{)}
        
        \PY{n}{np}\PY{o}{.}\PY{n}{median}\PY{p}{(}\PY{n}{ECS}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}7}]:} 2.7015459720884865
\end{Verbatim}
            
    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}8}]:} \PY{k+kn}{from} \PY{n+nn}{scipy}\PY{n+nn}{.}\PY{n+nn}{stats} \PY{k}{import} \PY{n}{lognorm}
        \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{between}
        
        \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
        \PY{n}{cond} \PY{o}{=} \PY{n}{between}\PY{p}{(}\PY{n}{ECS}\PY{p}{,} \PY{l+m+mi}{0}\PY{p}{,} \PY{l+m+mi}{10}\PY{p}{)}
        \PY{n}{sns}\PY{o}{.}\PY{n}{distplot}\PY{p}{(}\PY{n}{ECS}\PY{p}{[}\PY{n}{cond}\PY{p}{]}\PY{p}{,} \PY{n}{bins}\PY{o}{=}\PY{l+m+mi}{100}\PY{p}{,} \PY{n}{label}\PY{o}{=}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{,} \PY{n}{kde}\PY{o}{=}\PY{k+kc}{False}\PY{p}{,} \PY{n}{fit}\PY{o}{=}\PY{n}{lognorm}\PY{p}{)}
        \PY{n}{plt}\PY{o}{.}\PY{n}{title}\PY{p}{(}\PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS median=}\PY{l+s+s2}{\PYZob{}}\PY{l+s+s2}{np.median(ECS[cond]):.2f\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
        \PY{n}{plt}\PY{o}{.}\PY{n}{xlim}\PY{p}{(}\PY{l+m+mi}{0}\PY{p}{,} \PY{l+m+mi}{6}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}8}]:} (0, 6)
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_17_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{historical-warming}{%
\subsection{Historical warming}\label{historical-warming}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}9}]:} \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{forcing\PYZus{}co2}\PY{p}{,} \PY{n}{delta\PYZus{}temp}\PY{p}{,} \PY{n}{delta\PYZus{}forcing}\PY{p}{,} \PY{n}{N1}\PY{p}{,} \PY{n}{N2}
        
        \PY{k}{with} \PY{n}{pm}\PY{o}{.}\PY{n}{Model}\PY{p}{(}\PY{p}{)} \PY{k}{as} \PY{n}{model}\PY{p}{:}
            \PY{n}{df2x} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{forcing\PYZus{}co2}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}F\PYZus{}}\PY{l+s+si}{\PYZob{}2x\PYZcb{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dDelta\PYZus{}T} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{delta\PYZus{}temp}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{Delta T\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dDelta\PYZus{}F} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{delta\PYZus{}forcing}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{Delta F\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dN1} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{N1}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}N\PYZus{}1\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dN2} \PY{o}{=} \PY{n}{normal\PYZus{}dist}\PY{p}{(}\PY{n}{N2}\PY{p}{,} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZdl{}N\PYZus{}2\PYZdl{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
            \PY{n}{dDelta\PYZus{}N} \PY{o}{=} \PY{n}{dN2} \PY{o}{\PYZhy{}} \PY{n}{dN1}
            \PY{n}{ECS} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{Deterministic}\PY{p}{(}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{,} \PY{p}{(}\PY{n}{df2x}\PY{o}{*}\PY{n}{dDelta\PYZus{}T}\PY{p}{)} \PY{o}{/} \PY{p}{(}\PY{n}{dDelta\PYZus{}F} \PY{o}{\PYZhy{}} \PY{n}{dDelta\PYZus{}N}\PY{p}{)}\PY{p}{)}
        
        \PY{n}{model}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}9}]:}
    
    $$
            \begin{array}{rcl}
            \text{$F_{2x}$} &\sim & \text{Normal}(\mathit{mu}=3.7,~\mathit{sd}=0.3)\\\text{$\Delta T$} &\sim & \text{Normal}(\mathit{mu}=0.77,~\mathit{sd}=0.08)\\\text{$\Delta F$} &\sim & \text{Normal}(\mathit{mu}=2.16,~\mathit{sd}=0.59)\\\text{$N_1$} &\sim & \text{Normal}(\mathit{mu}=0.15,~\mathit{sd}=0.075)\\\text{$N_2$} &\sim & \text{Normal}(\mathit{mu}=0.71,~\mathit{sd}=0.06)\\\text{ECS} &\sim & \text{Deterministic}(\text{$F_{2x}$},~\text{$\Delta T$},~\text{$\Delta F$},~\text{$N_2$},~\text{$N_1$})
            \end{array}
            $$

    

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}10}]:} \PY{k+kn}{import} \PY{n+nn}{os}
        \PY{k}{with} \PY{n}{model}\PY{p}{:}
            \PY{n}{trace} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{sample}\PY{p}{(}
                \PY{l+m+mi}{100}\PY{n}{\PYZus{}000}\PY{p}{,}
                \PY{n}{cores}\PY{o}{=}\PY{n}{os}\PY{o}{.}\PY{n}{cpu\PYZus{}count}\PY{p}{(}\PY{p}{)} \PY{o}{/}\PY{o}{/} \PY{l+m+mi}{2}
            \PY{p}{)}
\end{Verbatim}


    \begin{Verbatim}[commandchars=\\\{\}]
Auto-assigning NUTS sampler{\ldots}
Initializing NUTS using jitter+adapt\_diag{\ldots}
Multiprocess sampling (6 chains in 6 jobs)
NUTS: [\$N\_2\$, \$N\_1\$, \$\textbackslash{}Delta F\$, \$\textbackslash{}Delta T\$, \$F\_\{2x\}\$]
Sampling 6 chains: 100\%|██████████| 603000/603000 [07:18<00:00, 1373.78draws/s]

    \end{Verbatim}


    \hypertarget{sampled-values-and-their-distributions}{%
\subsubsection{Sampled values and their
distributions}\label{sampled-values-and-their-distributions}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}11}]:} \PY{o}{\PYZpc{}\PYZpc{}capture} \PYZhy{}\PYZhy{}no\PYZhy{}display
         \PY{o}{\PYZpc{}}\PY{n}{matplotlib} \PY{n}{inline}
         \PY{n}{axes} \PY{o}{=} \PY{n}{pm}\PY{o}{.}\PY{n}{traceplot}\PY{p}{(}\PY{n}{trace}\PY{p}{)}
\end{Verbatim}


    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_26_0.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}12}]:} \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
         \PY{n}{pm}\PY{o}{.}\PY{n}{plots}\PY{o}{.}\PY{n}{forestplot}\PY{p}{(}\PY{n}{trace}\PY{p}{,} \PY{n}{rhat}\PY{o}{=}\PY{k+kc}{False}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}12}]:} GridSpec(1, 1)
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_27_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{input-parameters}{%
\subsubsection{Input parameters}\label{input-parameters}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}13}]:} \PY{o}{\PYZpc{}\PYZpc{}capture} \PYZhy{}\PYZhy{}no\PYZhy{}display
         \PY{o}{\PYZpc{}}\PY{n}{matplotlib} \PY{n}{inline}
         
         \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
         \PY{k}{for} \PY{n}{var} \PY{o+ow}{in} \PY{n}{trace}\PY{o}{.}\PY{n}{varnames}\PY{p}{:}
             \PY{n}{value} \PY{o}{=} \PY{n}{trace}\PY{o}{.}\PY{n}{get\PYZus{}values}\PY{p}{(}\PY{n}{var}\PY{p}{)}
             \PY{k}{if} \PY{n}{var} \PY{o}{!=} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:}
                 \PY{n}{sns}\PY{o}{.}\PY{n}{distplot}\PY{p}{(}\PY{n}{value}\PY{p}{,} \PY{n}{label}\PY{o}{=}\PY{n}{var}\PY{p}{)}
         
         \PY{n}{plt}\PY{o}{.}\PY{n}{xlim}\PY{p}{(}\PY{o}{\PYZhy{}}\PY{l+m+mi}{2}\PY{p}{,} \PY{l+m+mi}{6}\PY{p}{)}
         \PY{n}{plt}\PY{o}{.}\PY{n}{legend}\PY{p}{(}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}13}]:} <matplotlib.legend.Legend at 0x7f9138458e10>
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_29_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{ecs}{%
\subsubsection{ECS}\label{ecs}}

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}14}]:} \PY{k+kn}{import} \PY{n+nn}{numpy} \PY{k}{as} \PY{n+nn}{np}
         \PY{n}{ECS} \PY{o}{=} \PY{n}{trace}\PY{o}{.}\PY{n}{get\PYZus{}values}\PY{p}{(}\PY{l+s+s1}{\PYZsq{}}\PY{l+s+s1}{ECS}\PY{l+s+s1}{\PYZsq{}}\PY{p}{)}
         
         \PY{n}{np}\PY{o}{.}\PY{n}{median}\PY{p}{(}\PY{n}{ECS}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}14}]:} 1.7734117656095303
\end{Verbatim}
            
    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}15}]:} \PY{k+kn}{from} \PY{n+nn}{scipy}\PY{n+nn}{.}\PY{n+nn}{stats} \PY{k}{import} \PY{n}{lognorm}
         \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{between}
         
         \PY{n}{plt}\PY{o}{.}\PY{n}{figure}\PY{p}{(}\PY{n}{dpi}\PY{o}{=}\PY{l+m+mi}{150}\PY{p}{)}
         \PY{n}{cond} \PY{o}{=} \PY{n}{between}\PY{p}{(}\PY{n}{ECS}\PY{p}{,} \PY{l+m+mi}{0}\PY{p}{,} \PY{l+m+mi}{10}\PY{p}{)}
         \PY{n}{sns}\PY{o}{.}\PY{n}{distplot}\PY{p}{(}\PY{n}{ECS}\PY{p}{[}\PY{n}{cond}\PY{p}{]}\PY{p}{,} \PY{n}{bins}\PY{o}{=}\PY{l+m+mi}{100}\PY{p}{,} \PY{n}{label}\PY{o}{=}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS}\PY{l+s+s2}{\PYZdq{}}\PY{p}{,} \PY{n}{kde}\PY{o}{=}\PY{k+kc}{False}\PY{p}{,} \PY{n}{fit}\PY{o}{=}\PY{n}{lognorm}\PY{p}{)}
         \PY{n}{plt}\PY{o}{.}\PY{n}{title}\PY{p}{(}\PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{ECS median=}\PY{l+s+s2}{\PYZob{}}\PY{l+s+s2}{np.median(ECS[cond]):.2f\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
         \PY{n}{plt}\PY{o}{.}\PY{n}{xlim}\PY{p}{(}\PY{l+m+mi}{0}\PY{p}{,} \PY{l+m+mi}{10}\PY{p}{)}
\end{Verbatim}


\begin{Verbatim}[commandchars=\\\{\}]
{\color{outcolor}Out[{\color{outcolor}15}]:} (0, 10)
\end{Verbatim}
            
    \begin{center}
    \adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_32_1.png}
    \end{center}
    { \hspace*{\fill} \\}
    
    \hypertarget{bibliography}{%
\subsection{Bibliography}\label{bibliography}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Salvatier, John, Thomas V. Wiecki, and Christopher Fonnesbeck. 2016.
  ``Probabilistic Programming in Python Using PyMC3.'' PeerJ Computer
  Science 2 (April): e55. https://doi.org/10.7717/peerj-cs.55.
\item
  Hoﬀman, Matthew D, and Andrew Gelman. n.d. ``The No-U-Turn Sampler:
  Adaptively Setting Path Lengths in Hamiltonian Monte Carlo,'' 31.
\end{enumerate}
