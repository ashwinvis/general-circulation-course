Equilibrium climate sensitivity (ECS) is the earth's long term response
to the doubling of \(CO_2\) in comparison with pre-industrial level.

\hypertarget{process-understanding-double-co_2-scenario}{%
\section{\texorpdfstring{Process understanding: Double \(CO_2\)
scenario}{Process understanding: Double CO\_2 scenario}}\label{process-understanding-double-co_2-scenario}}

Starting from the linearized energy balance equation about a stable
stationary state:

\[ N = F + \lambda T\]

    we can expect the planetary imbalance to reach the limit \(N \to 0\)
once the climate reaches an equilibrium.

\[0 = F + \lambda T \]

    Now in this context we can substitute \(ECS = T\) since we are
interested in the long-term response to the doubling of \(CO_2\), which
is represented by the forcing \(F = F_{2x}\) and the total feedback
\(\lambda\).

\[ 0 = F_{2x} + \lambda (ECS) \]

    \[ ECS = -\frac{F_{2x}}{\lambda} \]

    From table 1 we have the input parameters:

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}1}]:} \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{lambdas}\PY{p}{,} \PY{n}{forcing\PYZus{}co2}
        \PY{k+kn}{from} \PY{n+nn}{IPython}\PY{n+nn}{.}\PY{n+nn}{display} \PY{k}{import} \PY{n}{Math}\PY{p}{,} \PY{n}{Latex}
        
        \PY{n}{Latex}\PY{p}{(}\PY{p}{(}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{begin}\PY{l+s+si}{\PYZob{}align\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{F\PYZus{}}\PY{l+s+si}{\PYZob{}2x\PYZcb{}}\PY{l+s+s2}{ \PYZam{}= }\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}forcing\PYZus{}co2\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{lambda \PYZam{}= }\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{\PYZob{}}\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}lambdas\PYZcb{}}\PY{l+s+s2}{\PYZdq{}} \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{end}\PY{l+s+si}{\PYZob{}align\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}
        \PY{p}{)}\PY{p}{)}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}1}]:}
    
    \begin{align} F_{2x} &= 3.70+/-0.30 \\\lambda &= \{{'planck': -3.2+/-0.1, 'waterlapse': 1.15+/-0.15, 'albedo': 0.3+/-0.15, 'cloud': 0.38+/-0.32}\}\end{align}

    

    The nominal values and uncertainties can be added together as follows

\[(a \pm \Delta a) + (b \pm \Delta b) \approx (a + b) \pm (\Delta a + \Delta b)\]

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}2}]:} \PY{n}{lambda\PYZus{}total} \PY{o}{=} \PY{n+nb}{sum}\PY{p}{(}\PY{n+nb}{list}\PY{p}{(}\PY{n}{lambdas}\PY{o}{.}\PY{n}{values}\PY{p}{(}\PY{p}{)}\PY{p}{)}\PY{p}{)}
        \PY{n}{Math}\PY{p}{(}\PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{Total }\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{lambda = }\PY{l+s+si}{\PYZob{}lambda\PYZus{}total\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}2}]:}
    
    $\displaystyle Total \lambda = -1.4+/-0.4$

    

    Plugging in the best estimates for the feedback parameters and forcing
from Table 1 into the equation

    \[ ECS = -\frac{F_{2x}}{\lambda} \]

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}3}]:} \PY{n}{ECS} \PY{o}{=} \PY{o}{\PYZhy{}}\PY{n}{forcing\PYZus{}co2}\PY{o}{.}\PY{n}{nominal\PYZus{}value} \PY{o}{/} \PY{n}{lambda\PYZus{}total}\PY{o}{.}\PY{n}{nominal\PYZus{}value}
        \PY{n}{Math}\PY{p}{(}\PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZlt{}ECS\PYZgt{} = }\PY{l+s+si}{\PYZob{}ECS:.2f\PYZcb{}}\PY{l+s+s2}{ K}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}3}]:}
    
    $\displaystyle <ECS> = 2.70 K$

    

    \hypertarget{historical-record-warming-betwen-two-periods}{%
\section{Historical record warming: between two
periods}\label{historical-record-warming-betwen-two-periods}}

Once again, we start from

\[ N = F + \lambda T\]

and apply it for two periods 1 (year 1859-1882) and 2 (year 2005-2015)

\[ N_1 = F_1 + \lambda T_1\] \[ N_2 = F_2 + \lambda T_2\]

and take a difference

\[ \Delta N = \Delta F + \lambda \Delta T\]

We also use the expression that we saw in the previous section for
\(ECS\)

\[ 0 = F_{2x} + \lambda (ECS) \]

Assuming the feedback parameters remain constant in between a periods 1
and 2 and for the long term projection, we eliminate \(\lambda\) to
obtain:

\[\frac{\Delta N - \Delta F}{\Delta T} = \frac{-F_{2x}}{ECS}\]

    Which gives: \[ECS = \frac{F_{2x} \Delta T}{\Delta F - \Delta N} \]

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}4}]:} \PY{k+kn}{from} \PY{n+nn}{util} \PY{k}{import} \PY{n}{forcing\PYZus{}co2}\PY{p}{,} \PY{n}{delta\PYZus{}temp}\PY{p}{,} \PY{n}{delta\PYZus{}forcing}\PY{p}{,} \PY{n}{delta\PYZus{}N}
        
        \PY{n}{Latex}\PY{p}{(}\PY{p}{(}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{begin}\PY{l+s+si}{\PYZob{}align\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{F\PYZus{}}\PY{l+s+si}{\PYZob{}2x\PYZcb{}}\PY{l+s+s2}{ \PYZam{}= }\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}forcing\PYZus{}co2\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{Delta T \PYZam{}=}\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}delta\PYZus{}temp\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{Delta F \PYZam{}=}\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}delta\PYZus{}forcing\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+sa}{r}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+se}{\PYZbs{}\PYZbs{}}\PY{l+s+s2}{Delta N \PYZam{}=}\PY{l+s+s2}{\PYZdq{}} \PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+si}{\PYZob{}delta\PYZus{}N\PYZcb{}}\PY{l+s+s2}{ }\PY{l+s+s2}{\PYZdq{}}
            \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZbs{}}\PY{l+s+s2}{end}\PY{l+s+si}{\PYZob{}align\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}
        \PY{p}{)}\PY{p}{)}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}4}]:}
    
    \begin{align} F_{2x} &= 3.70+/-0.30 \\\Delta T &=0.77+/-0.08 \\\Delta F &=2.2+/-0.6 \\\Delta N &=0.56+/-0.10 \end{align}

    

    Plugging in the best estimates for the inputs into the equation

    \[ECS = \frac{F_{2x} \Delta T}{\Delta F - \Delta N} \]

    we obtain

    \begin{Verbatim}[commandchars=\\\{\}]
{\color{incolor}In [{\color{incolor}5}]:} \PY{n}{ECS} \PY{o}{=} \PY{p}{(}
            \PY{n}{forcing\PYZus{}co2}\PY{o}{.}\PY{n}{nominal\PYZus{}value} \PY{o}{*} \PY{n}{delta\PYZus{}temp}\PY{o}{.}\PY{n}{nominal\PYZus{}value} \PY{o}{/} 
            \PY{p}{(}\PY{n}{delta\PYZus{}forcing}\PY{o}{.}\PY{n}{nominal\PYZus{}value} \PY{o}{\PYZhy{}} \PY{n}{delta\PYZus{}N}\PY{o}{.}\PY{n}{nominal\PYZus{}value}\PY{p}{)}
        \PY{p}{)}
        \PY{n}{Math}\PY{p}{(}\PY{n}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{\PYZlt{}ECS\PYZgt{} = }\PY{l+s+si}{\PYZob{}ECS:.2f\PYZcb{}}\PY{l+s+s2}{ K}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\end{Verbatim}

\texttt{\color{outcolor}Out[{\color{outcolor}5}]:}
    
    $\displaystyle <ECS> = 1.78 K$
