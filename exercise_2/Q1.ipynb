{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Climate sensitivity exercise for General Circulation\n",
    "\n",
    "Equillibrium climate sensitivity (ECS) is the earth's long term response to the doubling of $CO_2$ in comparison with pre-industrial level. \n",
    "\n",
    "## Process understanding: Double $CO_2$ scenario\n",
    "\n",
    "Starting from the linearized energy balance equation about a stable stationary state:\n",
    "\n",
    "$$ N = F + \\lambda T$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "we can expect the planetary imbalance to reach the limit $N \\to 0$ once the climate reaches an equillibrium.\n",
    "\n",
    "$$0 = F + \\lambda T $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now in this context we can substitute $ECS = T$ since we are interested in the long-term response to the doubling of $CO_2$, which is represented by the forcing $F = F_{2x}$ and the total feedback $\\lambda$.\n",
    "\n",
    "$$ 0 = F_{2x} + \\lambda (ECS) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ ECS = -\\frac{F_{2x}}{\\lambda} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From table 1 we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{align} F_{2x} &= 3.70+/-0.30 \\\\\\lambda &= \\{{'planck': -3.2+/-0.1, 'waterlapse': 1.15+/-0.15, 'albedo': 0.3+/-0.15, 'cloud': 0.38+/-0.32}\\}\\end{align}"
      ],
      "text/plain": [
       "<IPython.core.display.Latex object>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from util import lambdas, forcing_co2\n",
    "from IPython.display import Math, Latex\n",
    "\n",
    "Latex((\n",
    "    r\"\\begin{align} \"\n",
    "    \"F_{2x} &= \" f\"{forcing_co2} \\\\\"\n",
    "    r\"\\\\lambda &= \\{\" f\"{lambdas}\" \"\\}\"\n",
    "    \"\\end{align}\"\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nominal values and uncertainties can be added together as follows\n",
    "\n",
    "$$(a \\pm \\Delta a) + (b \\pm \\Delta b) \\approx (a + b) \\pm (\\Delta a + \\Delta b)$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle Total \\lambda = -1.4+/-0.4$"
      ],
      "text/plain": [
       "<IPython.core.display.Math object>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "lambda_total = sum(list(lambdas.values()))\n",
    "Math(f\"Total \\lambda = {lambda_total}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plugging in the best estimates for the feedback parameters and forcing from Table 1 into the equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ ECS = -\\frac{F_{2x}}{\\lambda} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle <ECS> = 2.70 K$"
      ],
      "text/plain": [
       "<IPython.core.display.Math object>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ECS = -forcing_co2.nominal_value / lambda_total.nominal_value\n",
    "Math(f\"<ECS> = {ECS:.2f} K\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Historical record warming: betwen two periods\n",
    "\n",
    "Once again, we start from\n",
    "\n",
    "$$ N = F + \\lambda T$$\n",
    "\n",
    "and apply it for two periods 1 (year 1859-1882) and 2 (year 2005-2015)\n",
    "\n",
    "$$ N_1 = F_1 + \\lambda T_1$$\n",
    "$$ N_2 = F_2 + \\lambda T_2$$\n",
    "\n",
    "and take a difference\n",
    "\n",
    "$$ \\Delta N = \\Delta F + \\lambda \\Delta T$$\n",
    "\n",
    "We also use the expression that we saw in the previous section for $ECS$\n",
    " \n",
    "$$ 0 = F_{2x} + \\lambda (ECS) $$\n",
    "\n",
    "assuming the feedback parameters remaing constant in between a periods 1 and 2 and for the long term projection, we eliminate $\\lambda$ to obtain:\n",
    "\n",
    "$$\\frac{\\Delta N - \\Delta F}{\\Delta T} = \\frac{-F_{2x}}{ECS}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which gives:\n",
    "$$ECS = \\frac{F_{2x} \\Delta T}{\\Delta F - \\Delta N} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "\\begin{align} F_{2x} &= 3.70+/-0.30 \\\\\\Delta T &=0.77+/-0.08 \\\\\\Delta F &=2.2+/-0.6 \\\\\\Delta N &=0.56+/-0.10 \\end{align}"
      ],
      "text/plain": [
       "<IPython.core.display.Latex object>"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from util import forcing_co2, delta_temp, delta_forcing, delta_N\n",
    "\n",
    "Latex((\n",
    "    r\"\\begin{align} \"\n",
    "    \"F_{2x} &= \" f\"{forcing_co2} \\\\\"\n",
    "    r\"\\\\Delta T &=\" f\"{delta_temp} \\\\\"\n",
    "    r\"\\\\Delta F &=\" f\"{delta_forcing} \\\\\"\n",
    "    r\"\\\\Delta N &=\" f\"{delta_N} \"\n",
    "    \"\\end{align}\"\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plugging in the best estimates for the inputs into the equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$ECS = \\frac{F_{2x} \\Delta T}{\\Delta F - \\Delta N} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "we obtain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle <ECS> = 1.78 K$"
      ],
      "text/plain": [
       "<IPython.core.display.Math object>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ECS = (\n",
    "    forcing_co2.nominal_value * delta_temp.nominal_value / \n",
    "    (delta_forcing.nominal_value - delta_N.nominal_value)\n",
    ")\n",
    "Math(f\"<ECS> = {ECS:.2f} K\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:general-circulation]",
   "language": "python",
   "name": "conda-env-general-circulation-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
