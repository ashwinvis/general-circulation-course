{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> by Ashwin Vishnu Mohanan (avmo@kth.se)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Derive an expression for the buoyancy frequency N (z) in the ocean \n",
    "\n",
    "*(see Vallis section 2.10 or Gill section 3.6).*\n",
    "\n",
    "\n",
    "The Brunt Vais\\\"ala frequency or the buoyancy frequency $N(z)$ is defined as\n",
    "the frequency at which a small fluid parcel might oscillate when displaced from\n",
    "the stable stratification.\n",
    "Following Vallis section 2.10, when a fluid parcel is displaced from its initial\n",
    "position, instantly equillibrates with the surrounding pressure. If the upward\n",
    "pressure force is greater than the weight of the parcel, it rises upwards - the\n",
    "parcel is said to be _buoyant_. Conversely, and when the parcel \"cools\", the\n",
    "weight of the parcel exceeds the pressure force, the parcel sinks. This sort of\n",
    "oscillatory motion develops in a stable equillibrium."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Expression for $N(z)$ in the atmosphere\n",
    "\n",
    "![](brunt.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key feature in such motions is that we can assume the _potential density_ of the parcel to be materially conserved. For a general fluid, the Brunt-Vais\\\"ala frequency is derived by estimating the difference in density of a displaced parcel ($\\rho$) from the surroundings ($\\tilde\\rho$):\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\delta\\rho \n",
    "  &= \\rho(z + \\delta z) - \\tilde\\rho(z+\\delta z) \\\\\n",
    "  &=  \\rho_\\theta(z + \\delta z) - \\tilde\\rho_\\theta(z+\\delta z) && \\because\\text{density is equivalent to pot. density at equillibrium}\\\\\n",
    "  &=  \\rho_\\theta(z) - \\tilde\\rho_\\theta(z+\\delta z) && \\because\\text{pot. density is materially conserved}\\\\\n",
    "  &=  \\tilde\\rho_\\theta(z) - \\tilde\\rho_\\theta(z+\\delta z) && \\because\\text{parcel was in equillibrium with the surroundings}\\\\\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "and thus,\n",
    "\n",
    "$$\\delta \\rho = -\\frac{\\partial \\rho_\\theta}{\\partial z} \\delta z$$\n",
    "\n",
    "An oscillatory motion with a frequency $N$ and amplitude $A$ along the vertical direction may be represented as:\n",
    "\n",
    "$$ \\delta z = \\Re(A\\exp(iN t))$$\n",
    "\n",
    "which implies the acceleration\n",
    "\n",
    "$$ \\frac{\\partial^2 \\delta z}{\\partial t^2} = -N^2\\Re(A\\exp(iN t)) = -N^2 \\delta z $$\n",
    "\n",
    "Also from Newton's law and Archimedis' principle, the acceleration acting on the parcel:\n",
    "\n",
    "$$ \\rho \\frac{\\partial^2 \\delta z}{\\partial t^2} = \\tilde\\rho g - \\rho g  = -(\\delta\\rho) g$$\n",
    "\n",
    "Putting the above two together:\n",
    "\n",
    "$$ N^2 \\delta z = g \\frac{\\delta \\rho}{\\rho} = -\\frac{g}{\\rho}\\frac{\\partial \\rho_\\theta}{\\partial z} \\delta z$$\n",
    "\n",
    "We can assume $\\rho_\\theta \\approx \\rho$, since the parcel is in near equillibrium with the surroundings:\n",
    "\n",
    "$$ N^2 = -\\frac{g}{\\rho_\\theta}\\frac{\\partial \\rho_\\theta}{\\partial z} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Expression for $N(z)$ in the ocean\n",
    "Since ocean is composed of sea water, and is compressible, we need to incorporate this effect too.\n",
    "\n",
    "$% To derive an expression for this frequency, particularly for sea water, we require an equation of state. The equation of state is derived from the expression for potential density as:$\n",
    "\n",
    "$$\\rho_\\theta = \\rho(S, \\theta, p_R)$$\n",
    "\n",
    "Where, $p_R$ is a reference pressure level.\n",
    "\n",
    "The first-order Taylor expansion of density $\\rho$ about $p_R$ yields:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\rho(S, \\theta, p)\n",
    " & \\approx \\rho(S, \\theta, p_R) + (p-p_R)  \\left(\\frac{\\partial \\rho}{\\partial p}\\right)_{S,\\theta} \\\\\n",
    " & = \\rho_\\theta + (p-P_R) c_s^{-2} \\\\\n",
    " & = \\rho_\\theta + p c_s^{-2} \\\\\n",
    " &= \\rho_\\theta + (\\rho_0 g z) c_s^{-2}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "As explained in equations 1.142-1.145, the expression for speed of sound\n",
    "propagation using an adiabatic process and the hydrostatic balance $p = p_R -\n",
    "\\rho_0gz$ has been applied here. The reference pressure is taken as $p_R=0$\n",
    "at $z=0$. Therefore for a small change,\n",
    "\n",
    "$$\\delta\\rho = \\delta\\rho_\\theta + \\delta p c_s^{-2}$$\n",
    "\n",
    "Since potential density is conserved:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\delta\\rho_\\theta\n",
    "  &= 0 \\\\\n",
    "  &=\\delta\\rho - \\delta p c_s^{-2} \\\\\n",
    "  &= \\delta\\rho - (-\\rho g \\delta z) c_s^{-2} && \\because \\text{hydrostatic balance}\\\\\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Therefore the change in density of the parcel being displaced adiabatically should follow:\n",
    "\n",
    "$$\\left(\\frac{\\partial\\rho}{\\partial z}\\right)_{\\rho_\\theta} = -\\frac{\\rho g}{c_s^2}$$\n",
    "\n",
    "Again, we consider the difference in density with the surrounding fluid:\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\delta\\rho\n",
    "  &= \\left[\\frac{\\partial\\rho}{\\partial z}_{\\rho_\\theta} - \\frac{\\partial\\tilde\\rho}{\\partial z} \\right] \\delta z \\\\\n",
    "  &= \\left[-\\frac{\\rho g}{c_s^2} - \\frac{\\partial\\tilde\\rho}{\\partial z} \\right] \\delta z\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Invoking Newtons's law and acceleration $=N^2 dz$, again:\n",
    "\n",
    "$$ \\rho \\frac{\\partial^2 \\delta z}{\\partial t^2} =  -(\\delta\\rho) g = g\\left[-\\frac{\\rho g}{c_s^2} - \\frac{\\partial\\tilde\\rho}{\\partial z} \\right] \\delta z$$\n",
    "\n",
    "\n",
    "$$ \\frac{\\partial^2 \\delta z}{\\partial t^2} = -g\\left[\\frac{\\ g}{c_s^2} +  \\frac{1}{\\rho}\\frac{\\partial\\tilde\\rho}{\\partial z} \\right] \\delta z = -N^2 \\delta z$$\n",
    "\n",
    "Invoking $\\tilde\\rho \\approx \\rho$ argument, since the parcel is in near equillibrium with the surroundings:\n",
    "\n",
    "$$ N^2 = g\\left[\\frac{\\ g}{c_s^2} +  \\frac{1}{\\tilde\\rho}\\frac{\\partial\\tilde\\rho}{\\partial z} \\right] $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "authors": [
   "Ashwin Vishnu Mohanan"
  ],
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "title": "Exercise for General circulation 2018/19: Ocean circulation"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
