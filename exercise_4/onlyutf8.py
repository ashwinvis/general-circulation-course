#!/usr/bin/env python
"""Remove non-UTF8 characters from standard input or a list of file

Examples
--------
$ ./onlyutf8.py input.txt
$ ./onlyutf8.py < input.txt > output.txt

Alternatives
------------
# Perl's piconv
$ piconv -f cp1252 -t UTF-8 < input.txt > output.txt
# iconv / uconv
$ iconv -f utf-8 -t utf-8 -c input.md

# Grep
$ grep -axv '.*' input.txt

Notes
-----
You can then diff to see where the bad bytes are:
$ git diff --no-index --color-words <(iconv -f utf-8 -t utf-8 -c input.md) input.md

"""
import fileinput


with fileinput.input(mode="rb") as f:
    LINES = [line.decode('utf-8', 'ignore') for line in f]


print(''.join(LINES))
