#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""Pandoc filter to allow variable wrapping of LaTeX/pdf documents
through the wrapfig package.

Simply add a " {?}" tag to the end of the caption for the figure, where
? is an integer specifying the width of the wrap in inches. 0 will
cause the width of the figure to be used.

The MIT License (MIT)

Copyright (c) 2015 C. Scott Hartley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""
from pandocfilters import toJSONFilter, Image, RawInline, stringify
import re

FLAG_PAT = re.compile(r".*\{(\d+\.?\d?)\}")


def wrapfig(key, val, fmt, meta):
    if key == "Image":
        attrs, caption, target = val
        if FLAG_PAT.match(stringify(caption)):
            # Strip tag
            size = FLAG_PAT.match(caption[-1]["c"]).group(1)
            stripped_caption = caption[:-2]
            if fmt == "latex":
                latex_begin = r"\begin{wrapfigure}{r}{" + size + r"\textwidth}"
                if len(stripped_caption) > 0:
                    latex_fig = (
                        r"\centering\includegraphics{" + target[0] + r"}\caption{"
                    )
                    latex_end = r"}\end{wrapfigure}"
                    return (
                        [RawInline(fmt, latex_begin + latex_fig)]
                        + stripped_caption
                        + [RawInline(fmt, latex_end)]
                    )
                else:
                    latex_fig = r"\centering\includegraphics{" + target[0] + "}"
                    latex_end = r"\end{wrapfigure}"
                    return [RawInline(fmt, latex_begin + latex_fig)] + [
                        RawInline(fmt, latex_end)
                    ]
            else:
                return Image(attrs, stripped_caption, target)


if __name__ == "__main__":
    toJSONFilter(wrapfig)
