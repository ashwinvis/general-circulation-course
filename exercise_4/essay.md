---
title: General circulation course - boundary layer essay
author: Ashwin Vishnu Mohanan (avmo@kth.se)
date: 2019-02-10
geometry: margin=1cm
figureTitle: |
  Figure #
---

> *Write a short essay (no more than one page) reflecting on the role of
> boundary-layer processes on the general circulation and your thesis topic*



My thesis topic primarily deals with study of geophysical turbulence and its
associated energetics. The main motivation behind this study is to answer the
question _``How many pressure levels are required for a model to sufficiently
replicate the spectra and energy fluxes, especially in the mesoscale range. Can
it be done by using the most extreme approximation, i.e. with a one-layer
model.''_ To this end, we simulate one-layer non-linear shallow water equations
and  to reach statistically stationary turbulence and then study the resulting
time-averaged spectra, structure functions and spectral fluxes.

Shallow water (SW) equations have been, for a long time, studied as a test case
for weather prediction models [@williamson_standard_1992]. SW models are a
viable candidate to study the dynamical processes in the atmosphere as it
permits both vortical motions and gravity-waves, unlike quasi-geostrophic
equations which solves only for the vortical component. In @fig:figure0 from
@lindborg_two-dimensional_2017-2 a comparison between the energy fluxes from a
GCM result (left) and simulation of a toy-model using a modified set of SW
equations (right) has been made. There are several interesting similarities
between the two models; for instance, we can notice that for wavelengths in the
mesoscale range, there is conversion from kinetic to available potential
energy. In the toy-model, being a more idealistic simulation, an equipartition
between K.E.  and A.P.E. fluxes is observed. One may infer that in mesoscale,
the energy cascade is dominated by wave-turbulence.

One can draw parallels from this description to boundary layer processes, which
interact with wave-turbulence [@sun_review_2015-1]. Such a study may be
significant in understanding and formulating scaling laws for resolved
gravity-wave drag on boundary layers flows. It also offers some insight into
how gravity-waves ``feed'' on the synoptic scales for energy.

Details which are missing from this picture are the stratification effects, a
model for the dissipation and the effect of topography. Stratification can be
included by adding more layers to the SW model and the limiting case would be
to solve the 3D Boussinesq equations. Typically, such simulations are
computationally intensive.

Viscous dissipation in SW model is a small-scale process and can be analogous
to surface drag in a boundary layer flow. In SW simulations, the value for
viscosity is set a priori depending on the available resolution and some
scaling estimates which usually includes the rate of forcing or the energy
input into the system. In the course, we had observed that by decreasing the
surface drag, more energy is accumulates in the GCM (slide 23) and via
baroclinic instabilities affects large-scale circulation.

Topography effects can be incorporated by adding a bottom boundary. However,
characterizing the roughness of the topography to be generic enough would be a
research question. Waves generated through wave-breaking and surface roughness
which in turn affects the boundary layer, can be studied through SW models to
some extent. 

To conclude, SW models and its variants can serve as a very simplified, yet
powerful tool to mimic dynamical processes which affects both the atmospheric
boundary layer and the large-scale circulation.


<div id="fig:figure0">
![](agu_gcm_flux.png){width=45%}
![](agu_run1_flux.png){width=45%}

Energy fluxes from a GCM compared with the same from a toy-model simulation
</div>


## Bibliography

