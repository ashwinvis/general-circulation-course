\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
  12pt,
]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={General circulation course - boundary layer essay},
  pdfauthor={Ashwin Vishnu Mohanan (avmo@kth.se)},
  pdfborder={0 0 0},
  breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage[margin=1cm]{geometry}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-2}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
  \let\oldparagraph\paragraph
  \renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
  \let\oldsubparagraph\subparagraph
  \renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

% \usepackage{wrapfig}
\usepackage{subfig}

\title{General circulation course - boundary layer essay}
\author{Ashwin Vishnu Mohanan (avmo@kth.se)}
\date{2019-02-10}

\begin{document}
\maketitle

\begin{quote}
\emph{Write a short essay (no more than one page) reflecting on the role
of boundary-layer processes on the general circulation and your thesis
topic}
\end{quote}

My thesis topic primarily deals with study of geophysical turbulence and
its associated energetics. The main motivation behind this study is to
answer the question \emph{``How many pressure levels are required for a
model to sufficiently replicate the spectra and energy fluxes,
especially in the mesoscale range. Can it be done by using the most
extreme approximation, i.e.~with a one-layer model.''} To this end, we
simulate one-layer non-linear shallow water equations and to reach
statistically stationary turbulence and then study the resulting
time-averaged spectra, structure functions and spectral fluxes.

Shallow water (SW) equations have been, for a long time, studied as a
test case for weather prediction models (Williamson et al. 1992). SW
models are a viable candidate to study the dynamical processes in the
atmosphere as it permits both vortical motions and gravity-waves, unlike
quasi-geostrophic equations which solves only for the vortical
component. In fig.~\ref{fig:figure0} from Lindborg and Mohanan (2017) a
comparison between the energy fluxes from a GCM result (left) and
simulation of a toy-model using a modified set of SW equations (right)
has been made. There are several interesting similarities between the
two models; for instance, we can notice that for wavelengths in the
mesoscale range, there is conversion from kinetic to available potential
energy. In the toy-model, being a more idealistic simulation, an
equipartition between K.E. and A.P.E. fluxes is observed. One may infer
that in mesoscale, the energy cascade is dominated by wave-turbulence.

One can draw parallels from this description to boundary layer
processes, which interact with wave-turbulence (Sun et al. 2015). Such a
study may be significant in understanding and formulating scaling laws
for resolved gravity-wave drag on boundary layers flows. It also offers
some insight into how gravity-waves ``feed'' on the synoptic scales for
energy.

Details which are missing from this picture are the stratification
effects, a model for the dissipation and the effect of topography.
Stratification can be included by adding more layers to the SW model and
the limiting case would be to solve the 3D Boussinesq equations.
Typically, such simulations are computationally intensive.

Viscous dissipation in SW model is a small-scale process and can be
analogous to surface drag in a boundary layer flow. In SW simulations,
the value for viscosity is set a priori depending on the available
resolution and some scaling estimates which usually includes the rate of
forcing or the energy input into the system. In the course, we had
observed that by decreasing the surface drag, more energy is accumulates
in the GCM (slide 23) and via baroclinic instabilities affects
large-scale circulation.

Topography effects can be incorporated by adding a bottom boundary.
However, characterizing the roughness of the topography to be generic
enough would be a research question. Waves generated through
wave-breaking and surface roughness which in turn affects the boundary
layer, can be studied through SW models to some extent.

To conclude, SW models and its variants can serve as a very simplified,
yet powerful tool to mimic dynamical processes which affects both the
atmospheric boundary layer and the large-scale circulation.

\begin{figure}
\centering

\subfloat[]{\includegraphics[width=0.45\textwidth,height=\textheight]{agu_gcm_flux.png}}
\subfloat[]{\includegraphics[width=0.45\textwidth,height=\textheight]{agu_run1_flux.png}}

\caption{Energy fluxes from a GCM compared with the same from a
toy-model simulation}

\label{fig:figure0}

\end{figure}

\hypertarget{bibliography}{%
\subsection*{Bibliography}\label{bibliography}}
\addcontentsline{toc}{subsection}{Bibliography}

\hypertarget{refs}{}
\leavevmode\hypertarget{ref-lindborg_two-dimensional_2017-2}{}%
Lindborg, Erik, and Ashwin Vishnu Mohanan. 2017. ``A Two-Dimensional Toy
Model for Geophysical Turbulence.'' \emph{Physics of Fluids} 29 (11):
111114. \url{https://doi.org/10.1063/1.4985990}.

\leavevmode\hypertarget{ref-sun_review_2015-1}{}%
Sun, Jielun, Carmen J. Nappo, Larry Mahrt, Danijel Belušić, Branko
Grisogono, David R. Stauffer, Manuel Pulido, et al. 2015. ``Review of
Wave-Turbulence Interactions in the Stable Atmospheric Boundary Layer:
WAVE-TURBULENCE INTERACTIONS.'' \emph{Reviews of Geophysics} 53 (3):
956--93. \url{https://doi.org/10.1002/2015RG000487}.

\leavevmode\hypertarget{ref-williamson_standard_1992}{}%
Williamson, David L., John B. Drake, James J. Hack, Rüdiger Jakob, and
Paul N. Swarztrauber. 1992. ``A Standard Test Set for Numerical
Approximations to the Shallow Water Equations in Spherical Geometry.''
\emph{Journal of Computational Physics} 102 (1): 211--24.
\url{https://doi.org/10.1016/S0021-9991(05)80016-6}.

\end{document}
