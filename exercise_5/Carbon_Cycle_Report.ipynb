{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Report by Ashwin Vishnu Mohanan (avmo@kth.se)**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The “biological pump” is a result of the primary production in the photic zone near the ocean surface. Explain how it affects the concentration of CO2 in the atmosphere."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](lea-bio-pump.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The biological pump consists of two components - _the soft-tissue_ and _the\n",
    "carbonate_ pump. These two components result in opposing behaviour and\n",
    "affects atmospheric CO$_2$ differently. The process in question here is the\n",
    "soft-tissue pump, which relies on the activity of the phytoplanktons near\n",
    "the ocean surface. Phytoplanktons are microbes equipped with chlorophyll\n",
    "to perform photosynthesis and are capable of synthesizing dissolved inorganic\n",
    "carbon into\n",
    "organic carbon ($C_{org}$) in the ocean as shown in green in the figure above\n",
    "(Hain et al. 2014). The biological soft-tissue pump's influence on the _concentration_ or\n",
    "partial pressure of carbon dioxide in the atmosphere, $pCO_2$ can\n",
    "be explained using the following relationships (see eq. 3a-3d in Hain et al. 2014):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{aligned}\n",
    "HCO_3^-   &\\approx 2DIC - ALK \\\\\n",
    "CO_3^{2-} &\\approx ALK - DIC \\\\\n",
    "pCO_2 \\propto \\frac{(HCO_3^-)^2}{CO_3^{2-}} &\\approx \\frac{(2DIC - ALK)^2}{ALK - DIC} \\\\\n",
    "\\end{aligned}                                             \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where $DIC$ and $ALK$ are dissolved inorganic carbon in and alkalinity of the sea water\n",
    "respectively. The soft tissue pump extracts $DIC$ from the water. As a consequence, the\n",
    "concentration of $CO_3^{2-}$ rises and $HCO_3^-$ declines. In other words $pCO_2$\n",
    "decreases and more $CO_2$ is pumped into the ocean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, and to summarize, this biological soft-tissue pump has a role of pumping $CO_2$ into the ocean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# In a large part of the ocean, the concentration of nutrients (e.g. phosphate) is very low near the surface as a result of the biological pump. What determines the net primary production (photosynthesis minus respiration) in these surface waters? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](lea-bio-cycle.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As shown in figure 3 from Hain et al. (2014), most of the  $C_{org}$\n",
    "that is generated through primary production by the\n",
    "phytoplanktons is consumed at a time scale which is less than a year\n",
    "and respired back into the atmosphere by zooplanktons and other marine life\n",
    "within the ocean surface.\n",
    "Around 10-50% of the organic matter generated evades the process of\n",
    "immediate respiration and rains into the ocean interior, termed as _export\n",
    "production_. Since not all\n",
    "$C_{org}$ generated in consumed by respiration, there is a net primary\n",
    "production by the biological pump.\n",
    "\n",
    "A tiny fraction of $C_{org}$ gets buried due to high pressure\n",
    "into the sea floor in the form of sedimentary rock, which these are\n",
    "virtually stuck and only released by weathering of rocks. \n",
    "\n",
    "A small, yet substantial fraction, is exported into _deep ocean_,\n",
    "where also respiration happens, but the excess $CO_2$ and nutrients\n",
    "regenerated has a longer residence time of $\\sim 1000$ years.\n",
    "The deep ocean also contains a resorvoir of preformed nutrients\n",
    "from ancient times.\n",
    "\n",
    "\n",
    "![](lea-bio-nutrient.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This nutrient rich subsurface water resurfaces\n",
    "due to upwelling ocean currents and ocean mixing typically near low-latitudes\n",
    "which have _low concentration of nutrients_, as shown above in the left loop.\n",
    "The black, green, orange, and blue lines are the nutrient, $C_{org}$, $CO_2$ \n",
    "and water transport respectively.\n",
    "Here, the biological pump performs\n",
    "at highest efficiency, with plenty of insolation for photosynthesis. The\n",
    "residual nutrients follows the surface ocean currents to higher\n",
    "latitude, high-nutrient surface regions, where it cools and sinks to the ocean\n",
    "interior.\n",
    "\n",
    "To summarize it is the _export production_ (i.e. sinking of $C_{org}$) and \n",
    "the supply of nutrients by ocean circulation which influence the net primary\n",
    "production of the biological pump."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# In the article “The biological pump in the past” (Hain et al), the “carbonate pump” is mentioned in section 8.18.2.2. Describe its effect on DIC, alkalinity and the atmospheric concentration of CO 2.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final product of the carbonate pump is export/precipitation of $CaCO_3$. This process depletes ALK and DIC in a 2:1 ratio, i.e. for every mole of $CaCO_3$ precipitated, exactly two moles of ALK and one mole of DIC is removed from the ocean. Using the relationships shown before"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{aligned}\n",
    "HCO_3^-   &\\approx 2DIC - ALK \\\\\n",
    "CO_3^{2-} &\\approx ALK - DIC \\\\\n",
    "pCO_2 \\propto \\frac{(HCO_3^-)^2}{CO_3^{2-}} &\\approx \\frac{(2DIC - ALK)^2}{ALK - DIC} \\\\\n",
    "\\end{aligned}                                             \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and considering the fact that the carbonate pump depletes the alkalinity of the sea water more than DIC, the consequence is that the concentration of $CO_3^{2-}$ decreases and that of $HCO_3^-$ increases. Due to the last proportionality relation in the equations above, it implies that the partial pressure of $CO_2$ the atmosphere would increase solely due to effect of the carbonate pump."
   ]
  }
 ],
 "metadata": {
  "authors": [
   "Ashwin Vishnu Mohanan"
  ],
  "kernelspec": {
   "display_name": "py-general-circulation-TiXNV1x2",
   "language": "python",
   "name": "py-general-circulation-tixnv1x2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "title": "Exercise for General circulation 2018/19: Carbon Cycle"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
